package com.strend.midleware.modal;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account {
    private Long id;

    private String name;
    private String email;
}
