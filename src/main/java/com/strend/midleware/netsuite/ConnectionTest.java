package com.strend.midleware.netsuite;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class ConnectionTest {
    public static void main(String[] args) throws Exception
    {
        Connection connection = null;
        try
        {

            Class.forName("com.netsuite.jdbc.openaccess.OpenAccessDriver");
            String connectionURL =
                    "jdbc:ns://4452538-sb1.connect.api.netsuite.com:1708;" +
                            "ServerDataSource=NetSuite2.com;" +
                            "Encrypted=1;" +
                            "NegotiateSSLClose=false;" +
                            "CustomProperties=(AccountID=4452538_SB1;RoleID=1187)";
            connection = DriverManager.getConnection(connectionURL, "dilesh.peiris8@gmail.com", "20CData24$");
            System.out.println("Connection success");
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM (SELECT * FROM transactionline) WHERE ROWNUM <= 10");
            while(rs.next()) {
                System.out.print(rs.getString(1));
                System.out.println();
            }
            ResultSetMetaData rsmd = rs.getMetaData();
            System.out.println("No. of columns : " + rsmd.getColumnCount());
            Set<String> dataTypes = new HashSet<>();
            for (int i = 1; i <=rsmd.getColumnCount(); i++) {

                dataTypes.add(rsmd.getColumnTypeName(i));
               // System.out.println("No. of columns : " + rsmd.getColumnCount() + "Column name of 1st column : " + rsmd.getColumnName(i) +"Column type of 1st column : " + rsmd.getColumnTypeName(i));
            }

            System.out.println(dataTypes);

//            ResultSet rs = stmt.executeQuery("SELECT count(*) from transactionline;");
//            System.out.println("Tables in the current database: ");
//            while(rs.next()) {
//                System.out.print(rs.getString(1));
//                System.out.println();
//            }
        }
        finally
        {
            if (connection != null)
                connection.close();
        }
    }
}
