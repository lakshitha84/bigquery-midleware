package com.strend.midleware.integrations;

import com.google.cloud.bigquery.*;
import com.google.common.collect.ImmutableMap;
import com.strend.midleware.modal.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public class BigQueryProxy {
    private final BigQuery bigQuery;


    @Autowired
    public BigQueryProxy(BigQuery bigQuery) {
        this.bigQuery = bigQuery;
    }

    public TableResult executeQuery(String query) throws InterruptedException {
        QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).build();
        Job queryJob = bigQuery.create(JobInfo.newBuilder(queryConfig).build());
        queryJob = queryJob.waitFor();

        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }

        return queryJob.getQueryResults();
    }


    public void insertData(Account account) throws InterruptedException {
        //DatasetId datasetId = DatasetId.of("netsuite");
        //TableId tableId = TableId.of("powerbi-data-warehouse", "netsuite", "account");
        TableId tableId = TableId.of("endless-beach-426315-j0", "NSDataConnect", "account");

        InsertAllRequest request = InsertAllRequest.newBuilder(tableId)
                .addRow(account.getId().toString(),
                        ImmutableMap.of("name", account.getName()))
                .build();

        InsertAllResponse response = bigQuery.insertAll(request);

        if (response.hasErrors()) {
            throw new RuntimeException("Error inserting data: " + response.getInsertErrors());
        }

        System.out.println("Data successfully inserted into BigQuery.");
    }

    public void createTable(String dataSetName, String tableName, List<Field> tableFields) {

        Schema schema = Schema.of(tableFields.toArray(new Field[]{}));
        StandardTableDefinition tableDefinition = StandardTableDefinition.of(schema);
        TableId tableId = TableId.of(dataSetName, tableName);
        TableInfo tableInfo = TableInfo.newBuilder(tableId, tableDefinition).build();
        bigQuery.create(tableInfo);
        System.out.println("Table " + tableName + " created successfully in dataset " + dataSetName);
    }

    public Boolean deleteTable(String datasetName, String tableName) {
        TableId tableId = TableId.of(datasetName, tableName);
        Table table = bigQuery.getTable(tableId);
        if (table != null) {
            return bigQuery.delete(tableId);
        }
        return Boolean.FALSE;
    }
}
