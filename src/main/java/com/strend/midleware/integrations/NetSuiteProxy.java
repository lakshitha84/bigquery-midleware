package com.strend.midleware.integrations;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;


@Slf4j
@Component
public class NetSuiteProxy {

    private Connection getConnection() {
        try {
            Class.forName("com.netsuite.jdbc.openaccess.OpenAccessDriver");
            String connectionURL =
                    "jdbc:ns://4452538-sb1.connect.api.netsuite.com:1708;" +
                            "ServerDataSource=NetSuite2.com;" +
                            "Encrypted=1;" +
                            "NegotiateSSLClose=false;" +
                            "CustomProperties=(AccountID=4452538_SB1;RoleID=1187)";
            Connection connection = DriverManager.getConnection(connectionURL, "dilesh.peiris8@gmail.com", "20CData24$");
            return  connection;
        } catch (Exception ex) {
            log.error("Error connecting to NetSuite", ex);
            throw new RuntimeException("Error connecting to database", ex);
        }
    }

    public Map<String, String>  getColumnNamesAndDataTypes(String tableName) {

        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM (SELECT * FROM transactionline) WHERE ROWNUM <= 10");
            ResultSetMetaData resultSetMetaData = rs.getMetaData();
            System.out.println("No. of columns : " + resultSetMetaData.getColumnCount());
            Map<String, String> columns = new HashMap<>();
            for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {

                columns.put(resultSetMetaData.getColumnName(i), resultSetMetaData.getColumnTypeName(i));
            }
            return columns;
        } catch (Exception ex) {
            throw new RuntimeException("Exception in getting column metadata for table {} " + tableName);
        }
    }
}
