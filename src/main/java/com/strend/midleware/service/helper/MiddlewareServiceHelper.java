package com.strend.midleware.service.helper;

import com.google.cloud.bigquery.Field;
import com.google.cloud.bigquery.LegacySQLTypeName;
import com.google.cloud.bigquery.Schema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MiddlewareServiceHelper {

    private Map<String, LegacySQLTypeName> dataTypeConversionMap = new HashMap<>();

    @Autowired
    public MiddlewareServiceHelper() {
        this.dataTypeConversionMap.put("BIGINT", LegacySQLTypeName.INTEGER);
        this.dataTypeConversionMap.put("DOUBLE", LegacySQLTypeName.FLOAT);
        this.dataTypeConversionMap.put("WVARCHAR", LegacySQLTypeName.STRING);
        this.dataTypeConversionMap.put("WCHAR", LegacySQLTypeName.STRING);
        this.dataTypeConversionMap.put("TIMESTAMP", LegacySQLTypeName.TIMESTAMP);
    }

    public List<Field> buildBigqueryTableFields(Map<String, String> columns) {

        List<Field> fields =
                columns.entrySet()
                .stream()
                .map((Map.Entry<String, String> entry) -> Field.of(entry.getKey(), dataTypeConversionMap.get(entry.getValue())))
                .collect(Collectors.toList());
        return fields;
    }




}
