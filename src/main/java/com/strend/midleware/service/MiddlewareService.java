package com.strend.midleware.service;


import com.google.cloud.bigquery.*;
import com.strend.midleware.integrations.BigQueryProxy;
import com.strend.midleware.integrations.NetSuiteProxy;
import com.strend.midleware.service.helper.MiddlewareServiceHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class MiddlewareService {

    private final NetSuiteProxy netSuiteProxy;
    private final BigQueryProxy bigQueryProxy;
    private final MiddlewareServiceHelper middlewareServiceHelper;
    @Value("${google.cloud.dataset.name}")
    private String dataSetName;
    public void createTable(String tableName) {

        bigQueryProxy.deleteTable(dataSetName, tableName);
        Map<String, String> columns = netSuiteProxy.getColumnNamesAndDataTypes(tableName);
        log.info("Columns and data type {} ", columns);
        List<Field> tableFields = middlewareServiceHelper.buildBigqueryTableFields(columns);
        bigQueryProxy.createTable(dataSetName, tableName, tableFields);
    }

    public void syncData(String tableName) {

    }
}
