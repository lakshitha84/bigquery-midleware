package com.strend.midleware.controller;

import com.google.api.services.bigquery.model.QueryParameter;
import com.google.cloud.bigquery.TableResult;
import com.strend.midleware.controller.request.TableCreateRequest;
import com.strend.midleware.controller.request.TableDeleteRequest;
import com.strend.midleware.integrations.BigQueryProxy;
import com.strend.midleware.modal.Account;
import com.strend.midleware.service.MiddlewareService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@RequiredArgsConstructor
public class BigQueryController {
    private final BigQueryProxy bigQueryService;

    private final MiddlewareService middlewareService;

    @Value("${google.cloud.dataset.name}")
    private String datasetName;



    @GetMapping("/query")
    public String executeQuery(@RequestParam String query) {
        try {
            TableResult
                    result = bigQueryService.executeQuery(query);
            StringBuilder response = new StringBuilder();
            result.iterateAll().forEach(row -> {
                row.forEach(val -> response.append(val.toString()).append(", "));
                response.append("\n");
            });
            return response.toString();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return "Query execution interrupted: " + e.getMessage();
        }
    }

    @PostMapping("/accounts")
    public ResponseEntity createAccount(@RequestBody Account account) {
        try {
            bigQueryService.insertData(account);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("accounts " + "account" + " created successfully in dataset " + "NSDataConnect");
    }

    @PostMapping("/table")
    public ResponseEntity createTable(@RequestBody TableCreateRequest request) {

        log.info("Request received to create table. Request : {}", request);
        middlewareService.createTable(request.getTableName());
        return ResponseEntity.ok("Table " + "account" + " created successfully in dataset " + "NSDataConnect");
    }

    @DeleteMapping("/table")
    public ResponseEntity deleteTable(@RequestBody TableDeleteRequest request) {
        log.info("Request received to create table. Request : {}", request);
        Boolean deleted = bigQueryService.deleteTable(datasetName, request.getTableName());
        return ResponseEntity.ok("Table " + "account" + " created successfully in dataset " + "NSDataConnect");
    }


}
