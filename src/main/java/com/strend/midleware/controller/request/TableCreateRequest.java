package com.strend.midleware.controller.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TableCreateRequest {

    private String tableName;
}
