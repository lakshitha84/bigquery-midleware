package com.strend.midleware.controller.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TableDeleteRequest {

    private String tableName;
}
